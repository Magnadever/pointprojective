﻿using System.Drawing;

namespace Point3D
{
    public class CPoint2D
    {
        public int x, y;
        public string name;
        public Brush color;

        public CPoint2D(int x, int y, string name, Brush color)
        {
            this.x = x;
            this.y = y;
            this.name = name;
            this.color = color;
        }
    }
}
