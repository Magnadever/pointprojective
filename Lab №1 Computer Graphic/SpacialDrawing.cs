﻿using System;
using System.Drawing;

namespace Point3D
{
    class SpacialDrawing
    {
        CPoint3D[] storageSpacial3D;
        CPoint2D[] storageSpacial2D;

        double alfa, betta, gamma;

        public SpacialDrawing()
        {
            storageSpacial3D = new CPoint3D[14];
            storageSpacial2D = new CPoint2D[14];
        }

        public void calculatePoints()
        {
            for (int i = 0; i < storageSpacial3D.Length; ++i)
            {
                    storageSpacial2D[i] = new CPoint2D(
                    (int)Math.Floor((double)Controller.xCenter - (double)storageSpacial3D[i].x * Math.Cos(alfa) -
                    (double)storageSpacial3D[i].y * Math.Cos(betta) - (double)storageSpacial3D[i].z * Math.Cos(gamma)),

                    (int)Math.Floor((double)Controller.yCenter + (double)storageSpacial3D[i].x * Math.Sin(alfa) + 
                    (double)storageSpacial3D[i].y * Math.Sin(betta) + (double)storageSpacial3D[i].z * Math.Sin(gamma)),

                    storageSpacial3D[i].name,
                    storageSpacial3D[i].color);
            }
        }

        public void updatePoints(ref CPoint3D[] storage, double alfa, double betta, double gamma)
        {
            storageSpacial3D = storage;
            this.alfa = alfa;
            this.betta = betta;
            this.gamma = gamma;
            calculatePoints();
        }

        public void drawPoints(Graphics g, Font fontNames, Pen colorLines)
        {
            _drawLines(g, colorLines);
            _drawPoints(g, fontNames);
        }

        void _drawLines(Graphics g, Pen colorLines)
        {
            g.DrawLine(colorLines, storageSpacial2D[0].x, storageSpacial2D[0].y, storageSpacial2D[4].x, storageSpacial2D[4].y); //T - T1
            g.DrawLine(colorLines, storageSpacial2D[0].x, storageSpacial2D[0].y, storageSpacial2D[5].x, storageSpacial2D[5].y); //T - T2
            g.DrawLine(colorLines, storageSpacial2D[0].x, storageSpacial2D[0].y, storageSpacial2D[6].x, storageSpacial2D[6].y); //T - T3
            g.DrawLine(colorLines, storageSpacial2D[4].x, storageSpacial2D[4].y, storageSpacial2D[2].x, storageSpacial2D[2].y); //T3 - Ty
            g.DrawLine(colorLines, storageSpacial2D[4].x, storageSpacial2D[4].y, storageSpacial2D[3].x, storageSpacial2D[3].y); //T3 - Tz
            g.DrawLine(colorLines, storageSpacial2D[5].x, storageSpacial2D[5].y, storageSpacial2D[1].x, storageSpacial2D[1].y); //T2 - Tx
            g.DrawLine(colorLines, storageSpacial2D[5].x, storageSpacial2D[5].y, storageSpacial2D[3].x, storageSpacial2D[3].y); //T2 - Tz
            g.DrawLine(colorLines, storageSpacial2D[6].x, storageSpacial2D[6].y, storageSpacial2D[1].x, storageSpacial2D[1].y); //T1 - Tx
            g.DrawLine(colorLines, storageSpacial2D[6].x, storageSpacial2D[6].y, storageSpacial2D[2].x, storageSpacial2D[2].y); //T1 - Ty
            g.DrawLine(colorLines, storageSpacial2D[1].x, storageSpacial2D[1].y, storageSpacial2D[7].x, storageSpacial2D[7].y); //Tx - O
            g.DrawLine(colorLines, storageSpacial2D[2].x, storageSpacial2D[2].y, storageSpacial2D[7].x, storageSpacial2D[7].y); //Ty - O
            g.DrawLine(colorLines, storageSpacial2D[3].x, storageSpacial2D[3].y, storageSpacial2D[7].x, storageSpacial2D[7].y); //Tz - O

            g.DrawLine(Controller.axisPen, storageSpacial2D[8].x, storageSpacial2D[8].y, storageSpacial2D[9].x, storageSpacial2D[9].y); //X
            g.DrawLine(Controller.axisPen, storageSpacial2D[10].x, storageSpacial2D[10].y, storageSpacial2D[11].x, storageSpacial2D[11].y); //Y
            g.DrawLine(Controller.axisPen, storageSpacial2D[12].x, storageSpacial2D[12].y, storageSpacial2D[13].x, storageSpacial2D[13].y); //Z
        }

        void _drawPoints(Graphics g, Font fontNames)
        {
            for (int i = 0; i < storageSpacial2D.Length; ++i)
            {
                g.FillEllipse(storageSpacial2D[i].color, storageSpacial2D[i].x - 4, storageSpacial2D[i].y - 4, 8, 8);
                g.DrawString(storageSpacial2D[i].name, fontNames, Brushes.Red, storageSpacial2D[i].x, storageSpacial2D[i].y);
            }
        }
    }
}
