﻿using System;
using System.Drawing;
using System.Windows.Forms; 

namespace Point3D
{
    public partial class Form1 : Form
    {
        CPoint3D[] baseStorage3DPoints = new CPoint3D[14];

        int x, y, z;
        double alfa, betta, gamma;

        public Form1()
        {
            InitializeComponent();
            Controller.Start(pictureBox1);
        }

        public void input()
        {
         
           alfa = convertToRadian(trackBarAlfa.Value);
           betta = convertToRadian(trackBarBeta.Value);
           gamma = convertToRadian(trackBarGamma.Value);
           createPoints();
        }

        public void processSpacial()
        {
            Controller.spacialDrawing.updatePoints(ref baseStorage3DPoints, alfa, betta, gamma);
        }

        public void processComplex()
        {
            Controller.complexDrawing.updatePoints(ref baseStorage3DPoints);
        }

        public void outputSpacial(Graphics g)
        {
            Controller.spacialDrawing.drawPoints(g, Controller.font, Pens.Blue);
        }

        public void outputComplex(Graphics g)
        {
            Controller.complexDrawing.drawPoints(g, Controller.font, Pens.Blue);
        }

  
        public void createPoints()
        {
            int offset = Controller.xCenter / 5;
            int lenAxis = Controller.yCenter - offset;
            x = Convert.ToInt32(trackBarX.Value);
            y = Convert.ToInt32(trackBarY.Value);
            z = Convert.ToInt32(trackBarZ.Value);
 
            baseStorage3DPoints[0] = new CPoint3D(x, y, z, "T", Brushes.Red); ;

            baseStorage3DPoints[1] = new CPoint3D(x, 0, 0, "Tx", Brushes.Turquoise); ;

            baseStorage3DPoints[2] = new CPoint3D(0, y, 0, "Ty", Brushes.Lime);
 
            baseStorage3DPoints[3] = new CPoint3D(0, 0, z, "Tz", Brushes.Peru);

            baseStorage3DPoints[4] = new CPoint3D(0, y, z, "T3", Brushes.Green);

            baseStorage3DPoints[5] = new CPoint3D(x, 0, z, "T2", Brushes.Yellow);

            baseStorage3DPoints[6] = new CPoint3D(x, y, 0, "T1", Brushes.Gray);

            baseStorage3DPoints[7] = new CPoint3D(0, 0, 0, "O", Brushes.Blue);

            baseStorage3DPoints[8] = new CPoint3D(lenAxis, 0, 0, "X", Brushes.Blue);

            baseStorage3DPoints[9] = new CPoint3D(-lenAxis, 0, 0, "", Brushes.Blue);

            baseStorage3DPoints[10] = new CPoint3D(0, lenAxis, 0, "Y", Brushes.Blue);

            baseStorage3DPoints[11] = new CPoint3D(0, -lenAxis, 0, "", Brushes.Blue);

            baseStorage3DPoints[12] = new CPoint3D(0, 0, lenAxis, "Z", Brushes.Blue);

            baseStorage3DPoints[13] = new CPoint3D(0, 0, -lenAxis, "", Brushes.Blue);
        }

        public double convertToRadian(int angle)
        {
            return (double)(angle * Math.PI) / 180.0; ;
        }


        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
           Complexer(e.Graphics);
        }

        private void pictureBox2_Paint(object sender, PaintEventArgs e)
        {
           Complexer(e.Graphics, false);
        }

        public void Complexer(Graphics gr, bool is_3d = true)
        {
            input();
            if (is_3d)
            {
                processSpacial();
                outputSpacial(gr);
            }
            else
            {
                processComplex();
                outputComplex(gr);
            }
        }

        
        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            Refresher();
        }

        private void trackBarY_Scroll(object sender, EventArgs e)
        {
            Refresher();
        }

        private void trackBarZ_Scroll(object sender, EventArgs e)
        {
            Refresher();
        }

        public void Refresher()
        {
            curValueGamma.Text = trackBarGamma.Value.ToString();
            curValueBeta.Text = trackBarBeta.Value.ToString();
            curValueAlfa.Text = trackBarAlfa.Value.ToString();
            curValueX.Text = trackBarX.Value.ToString();
            curValueY.Text = trackBarY.Value.ToString();
            curValueZ.Text = trackBarZ.Value.ToString();
            pictureBox1.Refresh();
            pictureBox2.Refresh();
        }

        private void labelComplexDrawing_Click(object sender, EventArgs e)
        {

        }

        private void trackBarAlfa_Scroll(object sender, EventArgs e)
        {
            Refresher();
        }

        private void trackBarBeta_Scroll(object sender, EventArgs e)
        {
            Refresher();
        }

        private void trackBarGamma_Scroll(object sender, EventArgs e)
        {
            Refresher();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
