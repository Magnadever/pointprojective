﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Point3D
{
    static class Controller
    {
        public static int xCenter { get; set; }
        public static int yCenter { get; set; }
        public static Font font { get; set; }
        public static Brush DefBrush { get; set; }
        public static Pen axisPen { get; set; }
        public static SpacialDrawing spacialDrawing { get; set; }
        public static ComplexDrawing complexDrawing { get; set; }

        public static void Start(PictureBox pb3d)
        {
            xCenter = pb3d.Width / 2;
            yCenter = pb3d.Height / 2;
            font = new Font("Times New Roman", 16F);
            DefBrush = Brushes.Blue;
            axisPen = new Pen(Color.Black, 3);
            complexDrawing = new ComplexDrawing();
            spacialDrawing = new SpacialDrawing();
        }
    }
}
