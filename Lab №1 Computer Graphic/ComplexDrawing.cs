﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms; 

namespace Point3D
{
    class ComplexDrawing
    {
        CPoint3D[] storageComplex3D = new CPoint3D[14];
        CPoint2D[] storageComplex2D = new CPoint2D[12];

        public ComplexDrawing()
        { }

        public void calculatePoints()
        {
                storageComplex2D[0] = new CPoint2D(Convert.ToInt32((double)Controller.xCenter), Convert.ToInt32((double)Controller.yCenter), "O", Brushes.Blue);

                storageComplex2D[1] = new CPoint2D(Convert.ToInt32((double)Controller.xCenter - storageComplex3D[8].x), Convert.ToInt32((double)Controller.yCenter), "X", Brushes.Blue);

                storageComplex2D[2] = new CPoint2D(Convert.ToInt32((double)Controller.xCenter - storageComplex3D[9].x), Convert.ToInt32((double)Controller.yCenter), "Y", Brushes.Blue);

                storageComplex2D[3] = new CPoint2D(Convert.ToInt32((double)Controller.xCenter), Convert.ToInt32((double)Controller.yCenter - storageComplex3D[10].y), "Z", Brushes.Blue);

                storageComplex2D[4] = new CPoint2D(Convert.ToInt32((double)Controller.xCenter), Convert.ToInt32((double)Controller.yCenter - storageComplex3D[11].y), "Y", Brushes.Blue);

                storageComplex2D[5] = new CPoint2D(Convert.ToInt32((double)Controller.xCenter - storageComplex3D[5].x), Convert.ToInt32((double)Controller.yCenter + storageComplex3D[4].y), "T1", storageComplex3D[6].color);

                storageComplex2D[6] = new CPoint2D(Convert.ToInt32((double)Controller.xCenter - storageComplex3D[5].x), Convert.ToInt32((double)Controller.yCenter - storageComplex3D[5].z), "T2", storageComplex3D[5].color);

                storageComplex2D[7] = new CPoint2D(Convert.ToInt32((double)Controller.xCenter + storageComplex3D[6].y), Convert.ToInt32((double)Controller.yCenter - storageComplex3D[5].z), "T3", storageComplex3D[4].color);

                storageComplex2D[8] = new CPoint2D(Convert.ToInt32((double)Controller.xCenter - storageComplex3D[1].x), Convert.ToInt32((double)Controller.yCenter), "Tx", storageComplex3D[1].color);

                storageComplex2D[9] = new CPoint2D(Convert.ToInt32((double)Controller.xCenter + storageComplex3D[2].y), Convert.ToInt32((double)Controller.yCenter), "Ty", storageComplex3D[2].color);

                storageComplex2D[10] = new CPoint2D(Convert.ToInt32((double)Controller.xCenter), Convert.ToInt32((double)Controller.yCenter + storageComplex3D[2].y), "Ty", storageComplex3D[2].color);

                storageComplex2D[11] = new CPoint2D(Convert.ToInt32((double)Controller.xCenter), Convert.ToInt32((double)Controller.yCenter - storageComplex3D[3].z), "Tz", storageComplex3D[3].color);
        }

        public void updatePoints(ref CPoint3D[] storage)
        {
            storageComplex3D = storage;
            calculatePoints();
        }

        public void drawPoints(Graphics g, Font fontNames, Pen colorLines)
        {
            _drawLines(g, colorLines);
            _drawPoints(g, fontNames);
        }

        void _drawLines(Graphics g, Pen colorLines)
        {
            g.DrawLine(colorLines, storageComplex2D[5].x, storageComplex2D[5].y, storageComplex2D[8].x, storageComplex2D[8].y);
            g.DrawLine(colorLines, storageComplex2D[5].x, storageComplex2D[5].y, storageComplex2D[10].x, storageComplex2D[10].y);

            g.DrawLine(colorLines, storageComplex2D[6].x, storageComplex2D[6].y, storageComplex2D[8].x, storageComplex2D[8].y);
            g.DrawLine(colorLines, storageComplex2D[6].x, storageComplex2D[6].y, storageComplex2D[11].x, storageComplex2D[11].y);

            g.DrawLine(colorLines, storageComplex2D[7].x, storageComplex2D[7].y, storageComplex2D[11].x, storageComplex2D[11].y);
            g.DrawLine(colorLines, storageComplex2D[7].x, storageComplex2D[7].y, storageComplex2D[9].x, storageComplex2D[9].y);

            Pen axisPen = new Pen(Color.Black, 3);
            g.DrawLine(axisPen, storageComplex2D[1].x, storageComplex2D[1].y, storageComplex2D[2].x, storageComplex2D[2].y);
            g.DrawLine(axisPen, storageComplex2D[3].x, storageComplex2D[3].y, storageComplex2D[4].x, storageComplex2D[4].y);

            if (storageComplex2D[10].y < Controller.yCenter)
            {
                int d = 2 * Math.Abs(storageComplex2D[0].x - storageComplex2D[9].x);
                g.DrawArc(Pens.Blue, storageComplex2D[0].x - d / 2, storageComplex2D[0].y - d / 2, d, d, 180, 90);
            }
            else if (storageComplex2D[10].y > Controller.yCenter)
            {
                int d = 2 * Math.Abs(storageComplex2D[0].x - storageComplex2D[9].x);
                g.DrawArc(Pens.Blue, storageComplex2D[0].x - d / 2, storageComplex2D[0].y - d / 2, d, d, 0, 90);
            }
        }

        void _drawPoints(Graphics g, Font fontNames)
        {
            for (int i = 0; i < storageComplex2D.Length; ++i)
            {
                g.FillEllipse(storageComplex2D[i].color, storageComplex2D[i].x - 4, storageComplex2D[i].y - 4, 8, 8);
                g.DrawString(storageComplex2D[i].name, fontNames, Brushes.Red, storageComplex2D[i].x, storageComplex2D[i].y);
            }
        }

    }

}
