﻿namespace Point3D
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labelSpacialDrawing = new System.Windows.Forms.Label();
            this.labelComplexDrawing = new System.Windows.Forms.Label();
            this.labelX = new System.Windows.Forms.Label();
            this.labelY = new System.Windows.Forms.Label();
            this.labelZ = new System.Windows.Forms.Label();
            this.labelAlfa = new System.Windows.Forms.Label();
            this.labelBeta = new System.Windows.Forms.Label();
            this.labelGamma = new System.Windows.Forms.Label();
            this.trackBarX = new System.Windows.Forms.TrackBar();
            this.labelMaxX = new System.Windows.Forms.Label();
            this.labelMinX = new System.Windows.Forms.Label();
            this.labelCurrentX = new System.Windows.Forms.Label();
            this.curValueX = new System.Windows.Forms.Label();
            this.labelZeroX = new System.Windows.Forms.Label();
            this.trackBarY = new System.Windows.Forms.TrackBar();
            this.labelMinY = new System.Windows.Forms.Label();
            this.labelMaxY = new System.Windows.Forms.Label();
            this.labelZeroY = new System.Windows.Forms.Label();
            this.trackBarZ = new System.Windows.Forms.TrackBar();
            this.labelMinZ = new System.Windows.Forms.Label();
            this.labelMaxZ = new System.Windows.Forms.Label();
            this.labelZeroZ = new System.Windows.Forms.Label();
            this.labelCurrentY = new System.Windows.Forms.Label();
            this.labelCurrentZ = new System.Windows.Forms.Label();
            this.curValueY = new System.Windows.Forms.Label();
            this.curValueZ = new System.Windows.Forms.Label();
            this.buttonExit = new System.Windows.Forms.Button();
            this.trackBarAlfa = new System.Windows.Forms.TrackBar();
            this.trackBarBeta = new System.Windows.Forms.TrackBar();
            this.trackBarGamma = new System.Windows.Forms.TrackBar();
            this.labelCurrentAlfa = new System.Windows.Forms.Label();
            this.labelCurrentbeta = new System.Windows.Forms.Label();
            this.labelCurrentGamma = new System.Windows.Forms.Label();
            this.curValueAlfa = new System.Windows.Forms.Label();
            this.curValueBeta = new System.Windows.Forms.Label();
            this.curValueGamma = new System.Windows.Forms.Label();
            this.labelMinAlfa = new System.Windows.Forms.Label();
            this.labelMaxAlfa = new System.Windows.Forms.Label();
            this.labelMinGamma = new System.Windows.Forms.Label();
            this.labelMinBeta = new System.Windows.Forms.Label();
            this.labelMaxGamma = new System.Windows.Forms.Label();
            this.labelMaxBeta = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarAlfa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarBeta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarGamma)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(55, 32);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(550, 550);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(792, 32);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(550, 550);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox2_Paint);
            // 
            // labelSpacialDrawing
            // 
            this.labelSpacialDrawing.AutoSize = true;
            this.labelSpacialDrawing.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSpacialDrawing.Location = new System.Drawing.Point(170, 4);
            this.labelSpacialDrawing.Name = "labelSpacialDrawing";
            this.labelSpacialDrawing.Size = new System.Drawing.Size(304, 25);
            this.labelSpacialDrawing.TabIndex = 2;
            this.labelSpacialDrawing.Text = "Пространственный чертеж";
            // 
            // labelComplexDrawing
            // 
            this.labelComplexDrawing.AutoSize = true;
            this.labelComplexDrawing.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelComplexDrawing.Location = new System.Drawing.Point(959, 4);
            this.labelComplexDrawing.Name = "labelComplexDrawing";
            this.labelComplexDrawing.Size = new System.Drawing.Size(246, 25);
            this.labelComplexDrawing.TabIndex = 3;
            this.labelComplexDrawing.Text = "Комплексный чертеж";
            this.labelComplexDrawing.Click += new System.EventHandler(this.labelComplexDrawing_Click);
            // 
            // labelX
            // 
            this.labelX.AutoSize = true;
            this.labelX.Location = new System.Drawing.Point(52, 606);
            this.labelX.Name = "labelX";
            this.labelX.Size = new System.Drawing.Size(17, 13);
            this.labelX.TabIndex = 4;
            this.labelX.Text = "X:";
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(52, 676);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(17, 13);
            this.labelY.TabIndex = 5;
            this.labelY.Text = "Y:";
            // 
            // labelZ
            // 
            this.labelZ.AutoSize = true;
            this.labelZ.Location = new System.Drawing.Point(52, 746);
            this.labelZ.Name = "labelZ";
            this.labelZ.Size = new System.Drawing.Size(17, 13);
            this.labelZ.TabIndex = 6;
            this.labelZ.Text = "Z:";
            // 
            // labelAlfa
            // 
            this.labelAlfa.AutoSize = true;
            this.labelAlfa.Location = new System.Drawing.Point(789, 606);
            this.labelAlfa.Name = "labelAlfa";
            this.labelAlfa.Size = new System.Drawing.Size(17, 13);
            this.labelAlfa.TabIndex = 7;
            this.labelAlfa.Text = "α:";
            // 
            // labelBeta
            // 
            this.labelBeta.AutoSize = true;
            this.labelBeta.Location = new System.Drawing.Point(789, 676);
            this.labelBeta.Name = "labelBeta";
            this.labelBeta.Size = new System.Drawing.Size(16, 13);
            this.labelBeta.TabIndex = 8;
            this.labelBeta.Text = "β:";
            // 
            // labelGamma
            // 
            this.labelGamma.AutoSize = true;
            this.labelGamma.Location = new System.Drawing.Point(790, 746);
            this.labelGamma.Name = "labelGamma";
            this.labelGamma.Size = new System.Drawing.Size(16, 13);
            this.labelGamma.TabIndex = 9;
            this.labelGamma.Text = "γ:";
            // 
            // trackBarX
            // 
            this.trackBarX.Location = new System.Drawing.Point(75, 602);
            this.trackBarX.Maximum = 120;
            this.trackBarX.Minimum = -120;
            this.trackBarX.Name = "trackBarX";
            this.trackBarX.Size = new System.Drawing.Size(435, 45);
            this.trackBarX.TabIndex = 12;
            this.trackBarX.Value = 50;
            this.trackBarX.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // labelMaxX
            // 
            this.labelMaxX.AutoSize = true;
            this.labelMaxX.Location = new System.Drawing.Point(491, 634);
            this.labelMaxX.Name = "labelMaxX";
            this.labelMaxX.Size = new System.Drawing.Size(25, 13);
            this.labelMaxX.TabIndex = 13;
            this.labelMaxX.Text = "120";
            // 
            // labelMinX
            // 
            this.labelMinX.AutoSize = true;
            this.labelMinX.Location = new System.Drawing.Point(72, 634);
            this.labelMinX.Name = "labelMinX";
            this.labelMinX.Size = new System.Drawing.Size(28, 13);
            this.labelMinX.TabIndex = 14;
            this.labelMinX.Text = "-120";
            // 
            // labelCurrentX
            // 
            this.labelCurrentX.AutoSize = true;
            this.labelCurrentX.Location = new System.Drawing.Point(526, 602);
            this.labelCurrentX.Name = "labelCurrentX";
            this.labelCurrentX.Size = new System.Drawing.Size(76, 26);
            this.labelCurrentX.TabIndex = 15;
            this.labelCurrentX.Text = "Текущее \r\nзначение X = ";
            // 
            // curValueX
            // 
            this.curValueX.AutoSize = true;
            this.curValueX.Location = new System.Drawing.Point(597, 615);
            this.curValueX.Name = "curValueX";
            this.curValueX.Size = new System.Drawing.Size(19, 13);
            this.curValueX.TabIndex = 16;
            this.curValueX.Text = "50";
            // 
            // labelZeroX
            // 
            this.labelZeroX.AutoSize = true;
            this.labelZeroX.Location = new System.Drawing.Point(286, 631);
            this.labelZeroX.Name = "labelZeroX";
            this.labelZeroX.Size = new System.Drawing.Size(13, 13);
            this.labelZeroX.TabIndex = 17;
            this.labelZeroX.Text = "0";
            // 
            // trackBarY
            // 
            this.trackBarY.Location = new System.Drawing.Point(75, 672);
            this.trackBarY.Maximum = 120;
            this.trackBarY.Minimum = -120;
            this.trackBarY.Name = "trackBarY";
            this.trackBarY.Size = new System.Drawing.Size(435, 45);
            this.trackBarY.TabIndex = 18;
            this.trackBarY.Value = 50;
            this.trackBarY.Scroll += new System.EventHandler(this.trackBarY_Scroll);
            // 
            // labelMinY
            // 
            this.labelMinY.AutoSize = true;
            this.labelMinY.Location = new System.Drawing.Point(72, 704);
            this.labelMinY.Name = "labelMinY";
            this.labelMinY.Size = new System.Drawing.Size(28, 13);
            this.labelMinY.TabIndex = 19;
            this.labelMinY.Text = "-120";
            // 
            // labelMaxY
            // 
            this.labelMaxY.AutoSize = true;
            this.labelMaxY.Location = new System.Drawing.Point(491, 704);
            this.labelMaxY.Name = "labelMaxY";
            this.labelMaxY.Size = new System.Drawing.Size(25, 13);
            this.labelMaxY.TabIndex = 20;
            this.labelMaxY.Text = "120";
            // 
            // labelZeroY
            // 
            this.labelZeroY.AutoSize = true;
            this.labelZeroY.Location = new System.Drawing.Point(286, 704);
            this.labelZeroY.Name = "labelZeroY";
            this.labelZeroY.Size = new System.Drawing.Size(13, 13);
            this.labelZeroY.TabIndex = 21;
            this.labelZeroY.Text = "0";
            // 
            // trackBarZ
            // 
            this.trackBarZ.Location = new System.Drawing.Point(75, 742);
            this.trackBarZ.Maximum = 120;
            this.trackBarZ.Minimum = -120;
            this.trackBarZ.Name = "trackBarZ";
            this.trackBarZ.Size = new System.Drawing.Size(435, 45);
            this.trackBarZ.TabIndex = 22;
            this.trackBarZ.Value = 60;
            this.trackBarZ.Scroll += new System.EventHandler(this.trackBarZ_Scroll);
            // 
            // labelMinZ
            // 
            this.labelMinZ.AutoSize = true;
            this.labelMinZ.Location = new System.Drawing.Point(72, 773);
            this.labelMinZ.Name = "labelMinZ";
            this.labelMinZ.Size = new System.Drawing.Size(28, 13);
            this.labelMinZ.TabIndex = 23;
            this.labelMinZ.Text = "-120";
            // 
            // labelMaxZ
            // 
            this.labelMaxZ.AutoSize = true;
            this.labelMaxZ.Location = new System.Drawing.Point(491, 773);
            this.labelMaxZ.Name = "labelMaxZ";
            this.labelMaxZ.Size = new System.Drawing.Size(25, 13);
            this.labelMaxZ.TabIndex = 24;
            this.labelMaxZ.Text = "120";
            // 
            // labelZeroZ
            // 
            this.labelZeroZ.AutoSize = true;
            this.labelZeroZ.Location = new System.Drawing.Point(286, 774);
            this.labelZeroZ.Name = "labelZeroZ";
            this.labelZeroZ.Size = new System.Drawing.Size(13, 13);
            this.labelZeroZ.TabIndex = 25;
            this.labelZeroZ.Text = "0";
            // 
            // labelCurrentY
            // 
            this.labelCurrentY.AutoSize = true;
            this.labelCurrentY.Location = new System.Drawing.Point(526, 672);
            this.labelCurrentY.Name = "labelCurrentY";
            this.labelCurrentY.Size = new System.Drawing.Size(76, 26);
            this.labelCurrentY.TabIndex = 26;
            this.labelCurrentY.Text = "Текущее \r\nзначение Y = ";
            // 
            // labelCurrentZ
            // 
            this.labelCurrentZ.AutoSize = true;
            this.labelCurrentZ.Location = new System.Drawing.Point(526, 742);
            this.labelCurrentZ.Name = "labelCurrentZ";
            this.labelCurrentZ.Size = new System.Drawing.Size(76, 26);
            this.labelCurrentZ.TabIndex = 27;
            this.labelCurrentZ.Text = "Текущее \r\nзначение Z = ";
            // 
            // curValueY
            // 
            this.curValueY.AutoSize = true;
            this.curValueY.Location = new System.Drawing.Point(597, 685);
            this.curValueY.Name = "curValueY";
            this.curValueY.Size = new System.Drawing.Size(19, 13);
            this.curValueY.TabIndex = 28;
            this.curValueY.Text = "50";
            // 
            // curValueZ
            // 
            this.curValueZ.AutoSize = true;
            this.curValueZ.Location = new System.Drawing.Point(597, 755);
            this.curValueZ.Name = "curValueZ";
            this.curValueZ.Size = new System.Drawing.Size(19, 13);
            this.curValueZ.TabIndex = 29;
            this.curValueZ.Text = "60";
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(647, 739);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(117, 45);
            this.buttonExit.TabIndex = 30;
            this.buttonExit.Text = "Выйти из программы";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // trackBarAlfa
            // 
            this.trackBarAlfa.Location = new System.Drawing.Point(812, 602);
            this.trackBarAlfa.Maximum = 360;
            this.trackBarAlfa.Name = "trackBarAlfa";
            this.trackBarAlfa.Size = new System.Drawing.Size(435, 45);
            this.trackBarAlfa.TabIndex = 31;
            this.trackBarAlfa.Scroll += new System.EventHandler(this.trackBarAlfa_Scroll);
            // 
            // trackBarBeta
            // 
            this.trackBarBeta.Location = new System.Drawing.Point(812, 672);
            this.trackBarBeta.Maximum = 360;
            this.trackBarBeta.Name = "trackBarBeta";
            this.trackBarBeta.Size = new System.Drawing.Size(435, 45);
            this.trackBarBeta.TabIndex = 32;
            this.trackBarBeta.Value = 135;
            this.trackBarBeta.Scroll += new System.EventHandler(this.trackBarBeta_Scroll);
            // 
            // trackBarGamma
            // 
            this.trackBarGamma.Location = new System.Drawing.Point(812, 741);
            this.trackBarGamma.Maximum = 360;
            this.trackBarGamma.Name = "trackBarGamma";
            this.trackBarGamma.Size = new System.Drawing.Size(435, 45);
            this.trackBarGamma.TabIndex = 33;
            this.trackBarGamma.Value = 270;
            this.trackBarGamma.Scroll += new System.EventHandler(this.trackBarGamma_Scroll);
            // 
            // labelCurrentAlfa
            // 
            this.labelCurrentAlfa.AutoSize = true;
            this.labelCurrentAlfa.Location = new System.Drawing.Point(1266, 606);
            this.labelCurrentAlfa.Name = "labelCurrentAlfa";
            this.labelCurrentAlfa.Size = new System.Drawing.Size(76, 26);
            this.labelCurrentAlfa.TabIndex = 34;
            this.labelCurrentAlfa.Text = "Текущее \r\nзначение α = ";
            // 
            // labelCurrentbeta
            // 
            this.labelCurrentbeta.AutoSize = true;
            this.labelCurrentbeta.Location = new System.Drawing.Point(1266, 672);
            this.labelCurrentbeta.Name = "labelCurrentbeta";
            this.labelCurrentbeta.Size = new System.Drawing.Size(75, 26);
            this.labelCurrentbeta.TabIndex = 35;
            this.labelCurrentbeta.Text = "Текущее \r\nзначение β = ";
            // 
            // labelCurrentGamma
            // 
            this.labelCurrentGamma.AutoSize = true;
            this.labelCurrentGamma.Location = new System.Drawing.Point(1266, 742);
            this.labelCurrentGamma.Name = "labelCurrentGamma";
            this.labelCurrentGamma.Size = new System.Drawing.Size(75, 26);
            this.labelCurrentGamma.TabIndex = 36;
            this.labelCurrentGamma.Text = "Текущее \r\nзначение γ = ";
            // 
            // curValueAlfa
            // 
            this.curValueAlfa.AutoSize = true;
            this.curValueAlfa.Location = new System.Drawing.Point(1338, 619);
            this.curValueAlfa.Name = "curValueAlfa";
            this.curValueAlfa.Size = new System.Drawing.Size(13, 13);
            this.curValueAlfa.TabIndex = 37;
            this.curValueAlfa.Text = "0";
            // 
            // curValueBeta
            // 
            this.curValueBeta.AutoSize = true;
            this.curValueBeta.Location = new System.Drawing.Point(1338, 685);
            this.curValueBeta.Name = "curValueBeta";
            this.curValueBeta.Size = new System.Drawing.Size(25, 13);
            this.curValueBeta.TabIndex = 38;
            this.curValueBeta.Text = "135";
            // 
            // curValueGamma
            // 
            this.curValueGamma.AutoSize = true;
            this.curValueGamma.Location = new System.Drawing.Point(1338, 755);
            this.curValueGamma.Name = "curValueGamma";
            this.curValueGamma.Size = new System.Drawing.Size(25, 13);
            this.curValueGamma.TabIndex = 39;
            this.curValueGamma.Text = "270";
            // 
            // labelMinAlfa
            // 
            this.labelMinAlfa.AutoSize = true;
            this.labelMinAlfa.Location = new System.Drawing.Point(819, 634);
            this.labelMinAlfa.Name = "labelMinAlfa";
            this.labelMinAlfa.Size = new System.Drawing.Size(13, 13);
            this.labelMinAlfa.TabIndex = 40;
            this.labelMinAlfa.Text = "0";
            // 
            // labelMaxAlfa
            // 
            this.labelMaxAlfa.AutoSize = true;
            this.labelMaxAlfa.Location = new System.Drawing.Point(1227, 634);
            this.labelMaxAlfa.Name = "labelMaxAlfa";
            this.labelMaxAlfa.Size = new System.Drawing.Size(25, 13);
            this.labelMaxAlfa.TabIndex = 41;
            this.labelMaxAlfa.Text = "360";
            // 
            // labelMinGamma
            // 
            this.labelMinGamma.AutoSize = true;
            this.labelMinGamma.Location = new System.Drawing.Point(819, 776);
            this.labelMinGamma.Name = "labelMinGamma";
            this.labelMinGamma.Size = new System.Drawing.Size(13, 13);
            this.labelMinGamma.TabIndex = 42;
            this.labelMinGamma.Text = "0";
            // 
            // labelMinBeta
            // 
            this.labelMinBeta.AutoSize = true;
            this.labelMinBeta.Location = new System.Drawing.Point(819, 704);
            this.labelMinBeta.Name = "labelMinBeta";
            this.labelMinBeta.Size = new System.Drawing.Size(13, 13);
            this.labelMinBeta.TabIndex = 43;
            this.labelMinBeta.Text = "0";
            // 
            // labelMaxGamma
            // 
            this.labelMaxGamma.AutoSize = true;
            this.labelMaxGamma.Location = new System.Drawing.Point(1227, 776);
            this.labelMaxGamma.Name = "labelMaxGamma";
            this.labelMaxGamma.Size = new System.Drawing.Size(25, 13);
            this.labelMaxGamma.TabIndex = 44;
            this.labelMaxGamma.Text = "360";
            // 
            // labelMaxBeta
            // 
            this.labelMaxBeta.AutoSize = true;
            this.labelMaxBeta.Location = new System.Drawing.Point(1227, 704);
            this.labelMaxBeta.Name = "labelMaxBeta";
            this.labelMaxBeta.Size = new System.Drawing.Size(25, 13);
            this.labelMaxBeta.TabIndex = 45;
            this.labelMaxBeta.Text = "360";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(1376, 798);
            this.Controls.Add(this.labelMaxBeta);
            this.Controls.Add(this.labelMaxGamma);
            this.Controls.Add(this.labelMinBeta);
            this.Controls.Add(this.labelMinGamma);
            this.Controls.Add(this.labelMaxAlfa);
            this.Controls.Add(this.labelMinAlfa);
            this.Controls.Add(this.curValueGamma);
            this.Controls.Add(this.curValueBeta);
            this.Controls.Add(this.curValueAlfa);
            this.Controls.Add(this.labelCurrentGamma);
            this.Controls.Add(this.labelCurrentbeta);
            this.Controls.Add(this.labelCurrentAlfa);
            this.Controls.Add(this.trackBarGamma);
            this.Controls.Add(this.trackBarBeta);
            this.Controls.Add(this.trackBarAlfa);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.curValueZ);
            this.Controls.Add(this.curValueY);
            this.Controls.Add(this.labelCurrentZ);
            this.Controls.Add(this.labelCurrentY);
            this.Controls.Add(this.labelZeroZ);
            this.Controls.Add(this.labelMaxZ);
            this.Controls.Add(this.labelMinZ);
            this.Controls.Add(this.trackBarZ);
            this.Controls.Add(this.labelZeroY);
            this.Controls.Add(this.labelMaxY);
            this.Controls.Add(this.labelMinY);
            this.Controls.Add(this.trackBarY);
            this.Controls.Add(this.labelZeroX);
            this.Controls.Add(this.curValueX);
            this.Controls.Add(this.labelCurrentX);
            this.Controls.Add(this.labelMinX);
            this.Controls.Add(this.labelMaxX);
            this.Controls.Add(this.trackBarX);
            this.Controls.Add(this.labelGamma);
            this.Controls.Add(this.labelBeta);
            this.Controls.Add(this.labelAlfa);
            this.Controls.Add(this.labelZ);
            this.Controls.Add(this.labelY);
            this.Controls.Add(this.labelX);
            this.Controls.Add(this.labelComplexDrawing);
            this.Controls.Add(this.labelSpacialDrawing);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Лабораторная работа по компьютерной графике № 1 \"Точка в 3D пространстве\" Ильина " +
    "А.В. Группа: МО-211";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarAlfa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarBeta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarGamma)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSpacialDrawing;
        private System.Windows.Forms.Label labelComplexDrawing;
        private System.Windows.Forms.Label labelX;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelZ;
        private System.Windows.Forms.Label labelAlfa;
        private System.Windows.Forms.Label labelBeta;
        private System.Windows.Forms.Label labelGamma;
        private System.Windows.Forms.TrackBar trackBarX;
        private System.Windows.Forms.Label labelMaxX;
        private System.Windows.Forms.Label labelMinX;
        private System.Windows.Forms.Label labelCurrentX;
        private System.Windows.Forms.Label curValueX;
        private System.Windows.Forms.Label labelZeroX;
        private System.Windows.Forms.TrackBar trackBarY;
        private System.Windows.Forms.Label labelMinY;
        private System.Windows.Forms.Label labelMaxY;
        private System.Windows.Forms.Label labelZeroY;
        private System.Windows.Forms.TrackBar trackBarZ;
        private System.Windows.Forms.Label labelMinZ;
        private System.Windows.Forms.Label labelMaxZ;
        private System.Windows.Forms.Label labelZeroZ;
        private System.Windows.Forms.Label labelCurrentY;
        private System.Windows.Forms.Label labelCurrentZ;
        private System.Windows.Forms.Label curValueY;
        private System.Windows.Forms.Label curValueZ;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.TrackBar trackBarAlfa;
        private System.Windows.Forms.TrackBar trackBarBeta;
        private System.Windows.Forms.TrackBar trackBarGamma;
        private System.Windows.Forms.Label labelCurrentAlfa;
        private System.Windows.Forms.Label labelCurrentbeta;
        private System.Windows.Forms.Label labelCurrentGamma;
        private System.Windows.Forms.Label curValueAlfa;
        private System.Windows.Forms.Label curValueBeta;
        private System.Windows.Forms.Label curValueGamma;
        private System.Windows.Forms.Label labelMinAlfa;
        private System.Windows.Forms.Label labelMaxAlfa;
        private System.Windows.Forms.Label labelMinGamma;
        private System.Windows.Forms.Label labelMinBeta;
        private System.Windows.Forms.Label labelMaxGamma;
        private System.Windows.Forms.Label labelMaxBeta;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}

