﻿using System.Drawing;

namespace Point3D
{
    class CPoint3D : CPoint2D
    {
        public int z;
        public CPoint3D(int x, int y, int z, string name, Brush color) : base( x, y, name, color)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.name = name;
            this.color = color;
        }
    }
}
